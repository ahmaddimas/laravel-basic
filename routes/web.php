<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Register route resource
Route::resource('categories', 'CategoriesController')->except([
    'index', 'show', 'destroy'
]);
Route::get('/categories/{categories}/destroy', 'CategoriesController@destroy')->name('categories.destroy');

Route::resource('products', 'ProductsController')->except([
    'index', 'show', 'destroy'
]);
Route::get('/products/{products}/destroy', 'ProductsController@destroy')->name('products.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sendmail', 'HomeController@sendmail')->name('user.sendmail');
