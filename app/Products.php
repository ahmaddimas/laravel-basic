<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'name', 'price', 'category_id'
    ];

    /**
     * Get the category that owns the products.
     */
    public function category()
    {
        return $this->belongsTo(Categories::class);
    }
}
