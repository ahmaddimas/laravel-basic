<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesValidation;
use App\Http\Services\CategoriesService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{

    private $service = null;

    public function __construct(CategoriesService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('features.categories.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(CategoriesValidation $request)
    {
        try {
            $categories = $this->service->store($request->validated()['name']);
            Log::debug('created categories: ' . $categories);
            $request->session()->flash('message', 'Success create categories');
        } catch (Exception $e) {
            Log::error('Error store categories: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during storing data');
        }
        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $categories = $this->service->find($id);
        Log::debug('edit categories: ' . $categories);
        return view('features.categories.form', ['categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CategoriesValidation $request, $id)
    {
        try {
            $categories = $this->service->update($id, $request->validated());
            Log::debug('updated categories: ' . $categories);
            $request->session()->flash('message', 'Success update categories');
        } catch (Exception $e) {
            Log::error('Error store categories: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during update data');
        }
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            Log::debug('deleted categories with id: ' . $id);
            $this->service->destroy($id);
            $request->session()->flash('message', 'Success delete categories');
        } catch (Exception $e) {
            Log::error('Error store categories: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during delete data');
        }
        return redirect()->route('home');
    }
}
