<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Mail\SimpleMailtrap;
use App\Products;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $categories = Categories::all(['id', 'name']);
        $products = Products::with('category:id,name')->get(['id', 'name', 'price', 'category_id']);
        Log::debug(HomeController::class . ' product: ' . $products);
        return view('home', ['categories' => $categories, 'products' => $products]);
    }

    public function sendmail(Request $request)
    {
        try {
            $targetAddress = 'test@inbox.mailtrap.io';
            Log::debug('send email to: ' . $targetAddress);
            Mail::to($targetAddress)->send(new SimpleMailtrap());
            $request->session()->flash('message', 'Success sending email');
        } catch (Exception $e) {
            Log::error('Error sendmail: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occured during sendmail');
        }
        return redirect()->route('home');
    }
}
