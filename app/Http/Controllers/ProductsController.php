<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests\ProductValidation;
use App\Http\Services\ProductsService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{

    private $service = null;

    public function __construct(ProductsService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Categories::all(['id', 'name']);
        return view('features.products.form', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductValidation $request
     * @return Response
     */
    public function store(ProductValidation $request)
    {
        try {
            $products = $this->service->store($request->validated());
            Log::debug('created products: ' . $products);
            $request->session()->flash('message', 'Success create products');
        } catch (Exception $e) {
            Log::error('Error store products: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during storing data');
        }
        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $categories = Categories::all(['id', 'name']);
        $products = $this->service->find($id);
        Log::debug('edit products: ' . $products);
        return view('features.products.form', ['products' => $products, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductValidation $request
     * @param int $id
     * @return Response
     */
    public function update(ProductValidation $request, $id)
    {
        try {
            $products = $this->service->update($id, $request->validated());
            Log::debug('updated products: ' . $products);
            $request->session()->flash('message', 'Success update products');
        } catch (Exception $e) {
            Log::error('Error store products: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during update data');
        }
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            Log::debug('deleted products with id: ' . $id);
            $this->service->destroy($id);
            $request->session()->flash('message', 'Success delete products');
        } catch (Exception $e) {
            Log::error('Error store products: ' . $e->getMessage());
            $request->session()->flash('message', 'Some error occurred during delete data');
        }
        return redirect()->route('home');
    }
}
