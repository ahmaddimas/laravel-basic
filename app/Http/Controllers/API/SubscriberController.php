<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailValidation;
use App\Http\Resources\SubscriberCollection;
use App\Http\Services\SubscriberService;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class SubscriberController extends Controller
{

    protected $service = null;

    public function __construct(SubscriberService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return SubscriberCollection
     */
    public function index()
    {
        try {
            Log::debug('get all subscriber');
            $result = $this->service->findAll();
            return (new SubscriberCollection($result))->additional(['status_code' => 1]);
//            return response()->json(['status_code' => 1, 'data' => $result]);
        } catch (Exception $e) {
            Log::error('error get all subscriber: ' . $e->getMessage());
            return response()->json(['status_code' => 0, 'message' => 'Failed listing subscriber']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmailValidation $request
     * @return Response
     */
    public function store(EmailValidation $request)
    {
        try {
            Log::debug(SubscriberController::class . ': store subscriber');
            $email = $request->validated()['email'];
            $result = $this->service->store($email);
            if ($result) {
                $this->service->sendMail($email);
            }
            return response()->json(['status_code' => 1, 'message' => 'Success create subscriber']);
        } catch (Exception $e) {
            Log::error('error store subscriber: ' . $e->getMessage());
            return response()->json(['status_code' => 0, 'message' => 'Failed create subscriber']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            Log::debug(SubscriberController::class . ': destroy subscriber');
            $result = $this->service->destroy($id);
            return response()->json(['status_code' => 1, 'message' => 'Success delete subscriber']);
        } catch (Exception $e) {
            Log::error('error store subscriber: ' . $e->getMessage());
            return response()->json(['status_code' => 0, 'message' => 'Failed delete subscriber']);
        }
    }
}
