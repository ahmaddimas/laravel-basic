<?php


namespace App\Http\Services;


use App\Products;
use Illuminate\Support\Facades\Log;

class ProductsService
{
    public function findAll()
    {
        Log::debug('get all products');
        return Products::all(['id', 'name']);
    }

    public function find($id)
    {
        Log::debug('find products with id: ' . $id);
        return Products::find($id);
    }

    public function store($data)
    {
        Log::debug('store products with name: ' . $data['name']);
        return Products::insert($data);
    }

    public function update($id, $data)
    {
        Log::debug('update products with id: ' . $id);
        return Products::where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        Log::debug('destroy products with id: ' . $id);
        return Products::destroy($id);
    }
}