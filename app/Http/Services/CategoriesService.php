<?php


namespace App\Http\Services;


use App\Categories;
use Illuminate\Support\Facades\Log;

class CategoriesService
{
    public function findAll()
    {
        Log::debug('get all categories');
        return Categories::all(['id', 'name']);
    }

    public function find($id)
    {
        Log::debug('find categories with id: ' . $id);
        return Categories::find($id);
    }

    public function store($name)
    {
        Log::debug('store categories with name: ' . $name);
        return Categories::insert(['name' => $name]);
    }

    public function update($id, $data)
    {
        Log::debug('update categories with id: ' . $id);
        return Categories::where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        Log::debug('destroy categories with id: ' . $id);
        return Categories::destroy($id);
    }
}