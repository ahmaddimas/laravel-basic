<?php


namespace App\Http\Services;


use App\Mail\SubscriberMailing;
use App\Subscriber;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SubscriberService
{
    public function findAll()
    {
        Log::debug('get all subscriber');
        return Subscriber::all(['id', 'email']);
    }

    public function store($email)
    {
        Log::debug('store subscriber with email: ' . $email);
        return Subscriber::insert(['email' => $email]);
    }

    public function destroy($id)
    {
        Log::debug('destroy subscriber with id: ' . $id);
        return Subscriber::destroy($id);
    }

    public function sendMail($email)
    {
        try {
            Log::debug('send email to: ' . $email);
            Mail::to($email)->send(new SubscriberMailing());
            return true;
        } catch (Exception $e) {
            Log::error('Error sendmail: ' . $e->getMessage());
            return false;
        }
    }
}