<div class="card">
    <div class="card-header">
        <a href="{{route('categories.create')}}" class="btn btn-outline-primary">Create</a>
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped mt-3">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $u)
                <tr>
                    <td>{{$u->name}}</td>
                    <td class="w-25">
                        <a href="{{route('categories.edit', $u)}}" class="btn btn-outline-success">Edit</a>
                        <a href="{{route('categories.destroy', $u)}}" class="btn btn-outline-danger">Remove</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>