@component('mail::message')
# Introduction

Hello **{{$name}}**,  {{-- use double space for line break --}}
Thank you for subscribe!

@component('mail::button', ['url' => ''])
    Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
